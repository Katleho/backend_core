﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mShopping.Controllers.UserModule
{
    public class UserAuthenticateResponse
    {
        public string _token { get; set; }

        public UserAuthenticateResponse(string token)
        {
            _token = token;
        }
    }
}
