﻿using Microsoft.AspNetCore.Mvc;
using mShopping.Controllers.UserModule;
using mShopping.DAL;
using mShopping.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mShopping.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUser _Repository;

        public UserController(IUser repository) 
        {
            _Repository = repository;
        }
        // GET api/Register/5
        [HttpPost("Get")]
        public IActionResult Get([FromBody] UserDto dto)
        {
            var user =_Repository.GetUser(dto);

            if(user==null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        [HttpPost]
        [Route("Post")]
        public ActionResult Create([FromBody] UserDto dto)
        {
            try
            {
                _Repository.Register(dto);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
            
        }
    }
}
