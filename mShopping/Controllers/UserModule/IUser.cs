﻿using mShopping.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mShopping.Controllers.UserModule
{
    public interface IUser
    {
        void Register(UserDto dto);

        UserAuthenticateResponse GetUser(UserDto dto);
    }
}
