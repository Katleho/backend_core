﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using mShopping.DAL;
using mShopping.Dto;
using mShopping.Helpers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace mShopping.Controllers.UserModule
{
    public class UserRepository : IUser
    {
        private DataContext _context;
        private readonly IConfiguration _appsettings;

       
        public UserRepository(DataContext context, IConfiguration appsettings)
        {
            _context = context;
            _appsettings = appsettings;
        }

        public UserAuthenticateResponse GetUser(UserDto _dto)
        {
            var user = _context.User.Where(x=>x.LastName == _dto.LastName && x.Password==_dto.Password);

            UserDto dto = null;
            foreach(var u in user)
            {
                dto = new UserDto()
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                };
            }
            var token = GenToken(dto);

            return new UserAuthenticateResponse(token);
        }

        public void Register(UserDto dto)
        {
            var user = new User 
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Password = dto.Password,
                Teleohone = dto.Telephone,
                DateCreated = DateTime.Now
            };
            _context.User.Add(user);
            _context.SaveChanges();
        }

        
        private string GenToken(UserDto user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appsettings.GetValue<string>("JWTKey:Secret"));
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("username", user.LastName.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
