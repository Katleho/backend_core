﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mShopping.DAL
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Teleohone { get; set; }
        public string Password { get; set; }

        public DateTime DateCreated { get; set; }

        public bool Isdeleted { get; set; }

    }
}
