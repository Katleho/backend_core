﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mShopping.Dto
{
    public class UserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Telephone { get; set; }
        public string Password { get; set; }

        public DateTime DateCreated { get; set; }

        public bool Isdeleted { get; set; }

        public string Token { get; set; }
    }
}
